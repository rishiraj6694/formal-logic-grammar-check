
#Check if s is in the form Pn for natural number n
def isAtomic(s):
    return s[0] == "P" and s[1:].isdigit()

#Check if s has the same number of left and right parentheses
def balanced(s):
    parens = 0
    for i in range(len(s)):
        if s[i] == "(":
            parens += 1
        elif s[i] == ")":
            parens -= 1
        if parens < 0:
            return False
    return parens == 0

#Check if s is in one of the forms (xvy), (x^y), or (x>y) for balanced x, y
def hasCenter(s):
    for i in range(len(s)):
        if balanced(s[:i]) and balanced(s[i+1:]) and s[i] in {"v","^",">"}:
            return True
    return False

#Returns the index of the central connective in s
def center(s):
    parens = 0
    index = 0
    for i in range(len(s)):
        if balanced(s[:i]) and balanced(s[i+1:]) and s[i] in {"v","^",">"}:
            return i

def grammarCheck(s):
    if s == "":
        return False
    if isAtomic(s):
        return True
    elif balanced(s):
        if s[0:2] == "(~":
            return grammarCheck(s[2:-1])
        elif hasCenter(s[1:-1]):
            i = center(s[1:-1])
            return grammarCheck(s[1:i+1]) and grammarCheck(s[i+2:-1])
    return False

#Generate list of primes for use in Godel numbeing
N = 100000
p = [True]*N
p[0], p[1] = False, False
prime_list = []
for i in range(2, N):
    if p[i]:
        k = 2
        while i*k < N:
            p[i*k] = False
            k += 1
        prime_list.append(i)

#Returns a list of factors of n
def factor(n):
    factors = [0]*10000
    i = 0
    while i < 10000:
        if n == 1:
            return factors
        p = prime_list[i]
        if n%p == 0:
            factors[i] += 1
            n //= p
        else:
            i += 1

#Converts a string s to its Godel number.
def godelNumber(s):
    n = 1
    dictionary = {"P":0, "(":1, ")":2, "~":3, "^":4, "v":5, ">":6}
    for i in range(len(s)):
        if s[i] in dictionary.keys():
            n *= prime_list[i]**dictionary[s[i]]
        elif s[i].isdigit():
            n *= prime_list[i]**(int(s[i]) + 7)
        else:
            return 0
    return n

while True:
    s = input()
    print("Grammatical:", grammarCheck(s))
    print("Godel number:", godelNumber(s))
    print()
