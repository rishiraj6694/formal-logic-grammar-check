# Formal Logic Grammar Check

Python scripts that run a recursive grammar check on a string to verify whether it is a well-formed-formula of propositional logic. They also convert the string to a [Gödel number](https://en.wikipedia.org/wiki/G%C3%B6del_numbering) using a standard translation scheme.

## Prop_Logic_Grammar_Check

![](Media/Grammar_Check_Sample.png)

## List_Propositional_Strings

![](Media/Listing_Prop_Strings.png)

## Number_To_Prop_String

![](Media/Num_To_Prop_String.png)